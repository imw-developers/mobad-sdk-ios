# MobAdSDK - 1.7.5


The following instructions will guide you through to integration process of the MobAd SDK in your native iOS app.

## Installation
**Step 1.** Add these following lines in your `podfile`
```
pod 'MobAdSDK', '1.7.5'

```
**Step 2.** Add the following post-install hook at the end of your podfile.
This would allow the SDK to be able to present the view controller that display the ads in-app in case the notification was tapped
```
post_install do |installer|
    #Specify what and where has to be added
    targetName = 'MobAdSDK'
    
    # Key & Value to allow the use of UIApplication shared instance
    keyApplicationExtensionAPIOnly = 'APPLICATION_EXTENSION_API_ONLY'
    valueApplicationExtensionAPIOnly = 'NO'

    #Find the pod which should be affected
    targets = installer.pods_project.targets.select { |target| target.name == targetName }
    target = targets[0]

    #Do the job
    target.build_configurations.each do |config|
        config.build_settings[keyApplicationExtensionAPIOnly] = valueApplicationExtensionAPIOnly
    end
end
```
**Step 3.** Run pod install
```
pod install
```

## Integration

### Notification Content Extension
**Step 4.** Add a new `Notification Content Extension` target.

**Step 5.** Open the `info.plist` file of the newly created extension as source sode and change the value of the following attributes to:
```
<key>UNNotificationExtensionCategory</key>
<string>MobAdSDKNotificationCategoryGeneric</string>
<key>UNNotificationExtensionDefaultContentHidden</key>
<true/>
<key>UNNotificationExtensionInitialContentSizeRatio</key>
<real>0.1</real>
```
**Step 6.** Open the view controller in the newly created target and Set the class `MobAdNotificationViewController` as its superclass
```swift
import MobAdSDK

class NotificationViewController: MobAdNotificationViewController { }
```

### AppDelegate
**Step 7.** Initialize the SDK with the ID generated for the app by the MobAd team when application finishes launching.
This call would also trigger the Location permission alert for the user.
Tracking the user is set over long distances in order to keep the battery usage to the minimum.
```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
...
  MobAdSDK.shared.initializeWith(identifier: "MOBAD_APP_ID")
...
  return true
}
```

**Step 8.** Call the activate method right after the initialization call.
```swift
...
  MobAdSDK.shared.initializeWith(identifier: "MOBAD_APP_ID")
  MobAdSDK.shared.activate()
...
```

**Step 9.** Handle notification interaction in the AppDelegate by calling the SDK handler in the related `UserNotification` framework method
```swift
func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
  defer {
    completionHandler()
  }
  
  guard !MobAdSDK.shared.handle(response) else {
    return
  }
  // Propagate the notification handling
  ...
}
```

**Step 10.** Display the ad notification when application is in the foreground . 
If the notification is handled by the SDK **do not** call the `completionHandler`.
```swift
func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
  guard !MobAdSDK.shared.handleForegroundNotification(notification, completion: completionHandler) else {
    return
  }
  
  // Uncomment following line if you which no to display any other notifications
  /** completionHandler([]) */
  // OR uncomment the following block to display them
  /**
    if #available(iOS 14.0, *) {
      completionHandler([.sound, .list, .banner])
    } else {
      completionHandler([.sound, .alert])
    }
   */
   ...
}
```

### Register User
**Step 11.** To start receiving ads a user should be initiated by calling this method
```swift
MobAdSDK.shared.initiateUser(email: String?, password: String?, countryCode: String?, completion: ((_ success: Bool, _ error: MobAdError?) -> Void)?)
```
This method would create a unique user on the MobAd side, even without the need to have his email. 
Call this method whenever the user becomes aware of the existence of this new feature in the integrating app, and he chooses to opt-in.

**Sign out. ** Call this method to sign a user out. Signing a user out means that he will not be receiving any more ads either.
```swift
MobAdSDK.shared.signOutUser()
```

### Ad Service Status
The ad serice status can be enabled or disabled. If enabled the user will be getting ads otherwise all ad deliveries are turned off.
```swift
// To enable and disable service
MobAdSDK.shared.adService(activate: Bool, completion: @escaping (_ success: Bool, _ error: MobAdError?) -> Void)

// To get the current status
MobAdSDK.shared.adServiceActive: Bool
```

### User Interests
Automatically once a user is initiated for the first time in _Step 11_ he is registered in all the interests by default. However he is able to tailor his own interests later on. 


```swift
// To get all the interested defined in the MobAd ecosystem
MobAdSDK.shared.allInterests(completion: @escaping (_ interests: Interests?, _ error: MobAdError?) -> Void)

// To get the user interests
MobAdSDK.shared.getUserInterests(completion: ((_ success: Bool, _ subcategoriesIDs: [Int]?, _ error: MobAdError?) -> Void)?)

// To set the user interests
MobAdSDK.shared.setUserInterests(subcategoriesIds: [Int], completion: ((_ success: Bool, _ error: MobAdError?) -> Void)?)
```

### Language
The user can select the languages he wishes to receive ads in.

By default when the user is first initiated the language sent in the parameters will be the used to determine the language he wishes to receive the ads in. If this parameter remains `nil` then the language code would be determined implicitally in the sdk from the current locale.
```swift
MobAdSDK.shared.initiateUser(email:password:countryCode:languageCode: String?,completion:)
```

A user can have multiple languages selected later on by updating his user profile with the languages codes.
Note that the `languageCode` parameter is used to select the default language from the array of `preferredAdLanguagesCodes`.
```swift
MobAdSDK.shared.updateUserProfile(countryCode:languageCode: String?, preferredAdLanguagesCodes: [String]?, completion:)
```

Read user languages settings
```swift
// The default user language
MobAdSDK.shared.languageCode

// All the languages the user receives ads in
MobAdSDK.shared.preferredAdLanguages
```

To check all the languages the platform currently supports
```swift
MobAdSDK.shared.getSupportedLanguages(completion:)
```

### Maximum Ads Per Day
A user can set the maximum amount of ads he wants to receive daily.
The completion block will always return the actual "Maximum Ads Per Day" being set since there's a ceiling limit that cannot be exceeded.
```swift
// Read the value
MobAdSDK.shared.userMaximumAdsPerDay

// Update the value
MobAdSDK.shared.updateUserProfile(countryCode:languageCode:, maxAdsPerDay: Int, preferredAdLanguagesCodes:completion:)
```

## In-app Interstital Ad

In order to request an interstitial in-app advertisement.
```swift
// Takes a delay in seconds
MobAdSDK.shared.requestInAppInvasiveAd(0)
```

## Location
The SDK can only displays ads after a successful call event if it's in a background state taking advantage of the Location services that can awaken the app and make the call detection optimal.
Note that in case the user only gave location permission "while using the app" then a blue status bar will appear when the app is in the background.
That's why for a smooth experience the user needs to grant an "Always" access to the location changes.

### Implementation 

##### Step 1 –  Add the `location updates background mode` capability

##### Step 2 –  Add the following values to the application info.plist file
```
<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
<string>This app requires location data. It works best when access is allowed in the background.</string>
<key>NSLocationWhenInUseUsageDescription</key>
<string>This app requires location data. It works best when access is allowed in the background.</string>
```
##### Step 3 –  Ask for the location permission at any given point and activate the location monitoring
> Notice that the `activate(for events:)` is called also when application finishes launching with no parameters at all.
> The SDK keeps record of the recent events activated. If not provided any, it activated the recently used.
> As for the first  `activate` call it activates `call event` detection by default 
```swift
guard MobAdSDK.shared.canAskPermissionForAlwaysMonitoringLocation() else {
  return
}
MobAdSDK.shared.requestAlwaysAuthorizationForLocationMonitoring()
MobAdSDK.shared.activate(for: [.call, .locationChange])
```

## Syncronizing Ads
Implementing the 'syncronizing ads' features would allow the sdk to schedule ads based on their CAP.
Scheduled ads will have their media downloaded in the background when given the chance, to make sure that users with low internet speed can have the best experience when an ad is displayed.
The add scheduling and caching is triggered in three different ways:

- Application Finishes launching (In the `activate()` method)
- Background App Refresh
- Push Notifications (Silent) 

### Background App Refresh

##### Step 1 –  Support Background App Refresh ([guide](https://developer.apple.com/documentation/uikit/app_and_environment/scenes/preparing_your_ui_to_run_in_the_background/updating_your_app_with_background_app_refresh))
> Support Background App Refresh for your app, do the following:
>1.  Enable the background fetch capability in your app ().
>2.  Call the  [`setMinimumBackgroundFetchInterval(_:)`](https://developer.apple.com/documentation/uikit/uiapplication/1623100-setminimumbackgroundfetchinterva)  method of  [`UIApplication`](https://developer.apple.com/documentation/uikit/uiapplication)  at launch time.
>3.  Implement the  [`application(_:performFetchWithCompletionHandler:)`](https://developer.apple.com/documentation/uikit/uiapplicationdelegate/1623125-application)  method in your app delegate
```swift
/**
 Set the minimum background fetch interval
 */
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
  ...
  let twoHoursInSeconds: TimeInterval = 60*60*2
  application.setMinimumBackgroundFetchInterval(twoHoursInSeconds)
  ...
}

/**
 Call the sdk method to start caching. 
 Once the caching finishes check the status in the completion block
 to identify if there was any change.
 */
func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
  MobAdSDK.shared.syncAds(.backgroundFetch) { result in
    completionHandler(result)
  }
}
```
##### Step 2 –  Handle Background Session If app was terminated while performing the background app refresh ([guide](https://www.raywenderlich.com/3244963-urlsession-tutorial-getting-started#toc-anchor-017))

> Make sure to send the completion to the sdk so it would be able to resume the media saving process after the completion of a download 

```swift
func application(_ application: UIApplication,
                 handleEventsForBackgroundURLSession identifier: String,
                 completionHandler: @escaping () -> Void) {
  MobAdSDK.shared.backgroundSessionCompletionHandler = completionHandler
}
```

##### Step 3 – Set up an App Group

 > Set up a common app group for the `main app` and the `notification content extension` in order to share the cached media.
> Once app group is set modify the initialize method in your `application(_:didFinishLaunchingWithOptions:)` by sending the group name
```swift
MobAdSDK.shared.initializeWith(identifier: "MOBAD_APP_ID", group: "APP_GROUP_STRING")
```

### Silent Remote Notifications

##### Step 1 –  Add Remote Notification Capability
> Add the `Remote Notifications` capability 

##### Step 2 –  Register App For Push Notificaiton
> In application finishes launching add the following
```swift
...
UNUserNotificationCenter.current().getNotificationSettings { settings in
  guard settings.authorizationStatus == .authorized else { return }
  DispatchQueue.main.async {
    UIApplication.shared.registerForRemoteNotifications()
  }
}
...
```

##### Step 3 –  Register to MobAdSDK Silent Notifications
```swift
func application(_ application: UIApplication,
                 didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
  let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
  let token = tokenParts.joined()
  
  MobAdSDK.shared.registerRemoteNotifications(token: token, completion: nil)
}
```

##### Step 4 –  Handle Silent Push Notifications Send By MobAd
> The return value of the `handleRemoteNotification(payload:completion:)` determines wether the notification is targeting the MobAd library or not.
> However once this method is called it takes the opportunity anyways to set an ads schedule for the day hence the completion block result is still to be taken into consideration.
```swift
private var adBackgrounFetchResult: UIBackgroundFetchResult?
func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
  let dispatchGroup = DispatchGroup()
  dispatchGroup.notify(queue: .main) { in
    completionHandler(self?.adBackgrounFetchResult ?? .failed)
  }
  
  dispatchGroup.enter()
  let isHandledByMobAdSDK = MobAdSDK.shared.handleRemoteNotification(userInfo) { [weak self] result in
    dispatchGroup.leave()
    self?.adBackgrounFetchResult = result
  }
  
  guard !isHandledByMobAdSDK else {
    return
  } 
  
  ...
}
```
## Handling Ad Events

With MobAd you can handle several ad events: 

Like when an Advertisement is opened by the user (onAdOpened function), closed (onAdClosed function), or even when an ad for some reason fails to load  (onAdFailed function) and finally when the maximum ads to be opened set by the user is reached (onMaxAdsOpenedReached function).

##### Step 1 –  To handle ad events you’re going to need to implement MobAdStateDelegate protocol:
```
class MainViewController: UIViewController, MobAdStateDelegate {}

```


##### Step 2 – Then you’re going to have to add it’s corresponding functions:

```

class MainViewController: UIViewController, MobAdStateDelegate {
    func onAdOpened(state adState: AdState) {
       //Insert your code here 
    }
    
    func onAdClosed(state adState: AdState) {
       //Insert your code here 
    }
    
    func onAdFailed(state adState: AdState) {
       //Insert your code here 
    }

    func onMaxAdsOpenedReached(state adState: AdState) {
    //Insert your code here 
    }
    
    func viewIsVisible() -> Bool {
        return MobAdSDK.shared.isViewVisible(self)
    }
    
}

```


##### Step 3 – And then, of course assigning the listener on viewdidload 

```

  override func viewDidLoad() {
        MobAdSDK.shared.addListenerDelegate(self)
    }

```
 
   
##### Step 4 – And finally removing the listener on viewdiddisappear

```

  override func viewDidDisappear(_ animated: Bool) {
        MobAdSDK.shared.removeListenerDelegate(self)
    }

``` 

##### Conclusion – So everything would look like:

```

  class MainViewController: UIViewController, MobAdStateDelegate {

    override func viewDidLoad() {
        MobAdSDK.shared.addListenerDelegate(self)
    }
   
    func onAdOpened(state adState: AdState) {
       //Insert your code here 
    }
    
    func onAdClosed(state adState: AdState) {
       //Insert your code here 
    }
    
    func onAdFailed(state adState: AdState) {
       //Insert your code here 
    }

    func onMaxAdsOpenedReached(state adState: AdState) {
    //Insert your code here 
    }
    
    func viewIsVisible() -> Bool {
        return MobAdSDK.shared.isViewVisible(self)
    }

    override func viewDidDisappear(_ animated: Bool) {
        MobAdSDK.shared.removeListenerDelegate(self)
    }

}

``` 

## Example
Feel free to explore the sample project to clear any integration ambiguity, or contact us

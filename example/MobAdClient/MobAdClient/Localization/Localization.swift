//
//  Localization.swift
//  MobAdClient
//
//  Created by Marwan Toutounji on 3/31/20.
//  Copyright © 2020 Marwan Toutounji. All rights reserved.
//

import Foundation

func localizedString(key: String, defaultValue: String = "", comment: String = "", formatVariables: CVarArg...) -> String {
  let localized: String = Bundle.main.localizedString(forKey: key, value: defaultValue, table: nil)
  if formatVariables.count > 0 {
    return withVaList(formatVariables, {
      return NSString(format: localized, locale: Locale.current , arguments: $0) as String
    })
  }
  return localized
}

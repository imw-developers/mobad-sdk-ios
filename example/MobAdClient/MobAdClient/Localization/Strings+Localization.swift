//
//  Strings+Localization.swift
//  MobAdClient
//
//  Created by Marwan Toutounji on 3/31/20.
//  Copyright © 2020 Marwan Toutounji. All rights reserved.
//

import Foundation

extension String {
  struct localized {
    static var ads: String {
      return localizedString(key: "ads")
    }
    static var cancel: String {
      return localizedString(key: "cancel")
    }
    static var done: String {
      return localizedString(key: "done")
    }
    static var guest: String {
      return localizedString(key: "guest")
    }
    static var interests: String {
      return localizedString(key: "interests")
    }
    static var media: String {
      return localizedString(key: "media")
    }
    static var pleaseSignInGuest: String {
      return localizedString(key: "please_sign_in_guest")
    }
    static var recentAdsFetchDate: String {
      return localizedString(key: "recent_ads_fetch_date")
    }
    static var recentSyncDate: String {
      return localizedString(key: "recent_sync_date")
    }
    static var save: String {
      return localizedString(key: "save")
    }
    static var selectAll: String {
      return localizedString(key: "select_all")
    }
    static func signedInUserMessage(user: String) -> String {
      return String(format: localizedString(key: "signed_in_user_message"), user)
    }
    static var subcategories: String {
      return localizedString(key: "subcategories")
    }
    static var unselectAll: String {
      return localizedString(key: "unselect_all")
    }
    static var whatToDo: String {
      return localizedString(key: "what_to_do")
    }
  }
}

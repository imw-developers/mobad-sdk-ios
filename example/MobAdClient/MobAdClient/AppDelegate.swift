//
//  AppDelegate.swift
//  MobAdClient
//
//  Created by Marwan  on 7/25/19.
//  Copyright © 2019 Marwan Toutounji. All rights reserved.
//

import MobAdSDK
import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in }
    UNUserNotificationCenter.current().configureNotificationCategories()
    UNUserNotificationCenter.current().delegate = self
    
    Theme.apply()
    
    MobAdSDK.shared.initializeWith(
      identifier: "m0b@ds@mpl3@pp1d",
      group: "group.com.imagineworks.mobad.clientapp")
    MobAdSDK.shared.activate()
    
    let twoHoursInSeconds: TimeInterval = 60*60*2
    application.setMinimumBackgroundFetchInterval(twoHoursInSeconds)
    
    // Register for Push Notifications
    registerForPushNotificationsIfAvailable()
    
    return true
  }
  
  // MARK: Push Notifications
  //=========================
  // Registration To APNs
  //----------------------
  func application(_ application: UIApplication,
                   didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
    let token = tokenParts.joined()
    print("=> Device Token: \(token)")
    
    MobAdSDK.shared.registerRemoteNotifications(token: token, completion: nil)
  }
  
  func application(_ application: UIApplication,
                   didFailToRegisterForRemoteNotificationsWithError error: Error) {
    print("=> Failed to register for PN: \(error)")
  }
  
  private func registerForPushNotificationsIfAvailable() {
    UNUserNotificationCenter.current().getNotificationSettings { settings in
      guard settings.authorizationStatus == .authorized else { return }
      DispatchQueue.main.async {
        UIApplication.shared.registerForRemoteNotifications()
      }
    }
  }
  
  // Handling
  //----------------------
  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    let isHandledByMobAdSDK = MobAdSDK.shared.handleRemoteNotification(userInfo) { result in
      completionHandler(result ?? .failed)
    }
    
    if isHandledByMobAdSDK {
      scheduleNotificationForSilentPush()
    } else {
      // Proceed
    }
  }
  //=========================
  
  func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    MobAdSDK.shared.syncAdsInBackground { result in
      completionHandler(result)
    }
  }
  
  func application(_ application: UIApplication,
                   handleEventsForBackgroundURLSession identifier: String,
                   completionHandler: @escaping () -> Void) {
    MobAdSDK.shared.backgroundSessionCompletionHandler = completionHandler
  }
}

extension AppDelegate {
  func scheduleNotification(identifier: String, content: UNNotificationContent) {
    // find out what are the user's notification preferences
    UNUserNotificationCenter.current().getNotificationSettings { (settings) in
      // we're only going to create and schedule a notification
      // if the user has kept notifications authorized for this app
      guard settings.authorizationStatus == .authorized else {
        return
      }
      let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
      let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
      UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
  }
  
  func scheduleNotificationForSilentPush() {
    let content = UNMutableNotificationContent()
    content.title = "😶 Silent 🔇 Push 🔕"
    content.body = "🏋🏻🏋️🏋🏼🏋🏽🏋🏾🏋🏿"
    content.subtitle = "True Story 😊"
    content.sound = UNNotificationSound.default
    scheduleNotification(identifier: "s1l3nt", content: content)
  }
}

// MARK: - Notification Center Delegate
//=====================================
extension AppDelegate: UNUserNotificationCenterDelegate {
  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    defer {
      completionHandler()
    }
    
    guard !MobAdSDK.shared.handle(response) else {
      return
    }
    // If the action does not belong
  }

  func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    guard !MobAdSDK.shared.handleForegroundNotification(notification, completion: completionHandler) else {
      return
    }
    completionHandler([])
  }
}

// MARK: - Notification Center Add Ons
//====================================
extension UNUserNotificationCenter {
  struct Category {
    static let clientImageNotification = "clientImageNotificationExtension"
  }
  
  enum Action: String {
    case open = "open"
    case deactivate = "deactivate"
    case cancel = "cancel"
    
    var title: String {
      switch self {
      case .open:
        return "Open"
      case .deactivate:
        return "Deactivate Ads"
      case .cancel:
        return "Cancel"
      }
    }
  }
  
  func configureNotificationCategories() {
    let viewAction = UNNotificationAction(
      identifier: Action.open.rawValue,
      title: Action.open.title,
      options: [.foreground])
    let deactiveAction = UNNotificationAction(
      identifier: Action.deactivate.rawValue,
      title: Action.deactivate.title,
      options: [.destructive])
    let cancelAction = UNNotificationAction(
      identifier: Action.cancel.rawValue,
      title: Action.cancel.title,
      options: [.destructive])
    
    let notificationCategory = UNNotificationCategory(
      identifier: UNUserNotificationCenter.Category.clientImageNotification,
      actions: [viewAction, deactiveAction, cancelAction], intentIdentifiers: [], options: [])
    setNotificationCategories([notificationCategory])
  }
}

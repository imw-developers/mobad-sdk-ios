//
//  InterestsTableViewController.swift
//  MobAdClient
//
//  Created by Marwan  on 1/8/20.
//  Copyright © 2020 Marwan Toutounji. All rights reserved.
//

import UIKit

class InterestsTableViewController: UITableViewController {
  static var instance: InterestsTableViewController {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    return storyboard.instantiateViewController(withIdentifier: "InterestsTableViewController") as! InterestsTableViewController
  }
  
  let viewModel = InterestsViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    startLoader()
    viewModel.loadInterests { success in
      defer {
        self.stopLoader()
        self.tableView.reloadData()
      }
      guard success else {
        // Display error if needed
        return
      }
    }
  }
  
  // MARK: - Table view data source
  override func numberOfSections(in tableView: UITableView) -> Int {
    return viewModel.numberOfSections
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRowsIn(section: section)
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
    let info = viewModel.informationForCellAtIndexPath(indexPath)
    cell.textLabel?.text = info?.title
    let shouldSelect = info?.isSelected ?? false
    cell.isSelectedStyle = shouldSelect
    if shouldSelect {
      tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
    } else {
      tableView.deselectRow(at: indexPath, animated: false)
    }
    return cell
  }
  
  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let view = TableViewSectionHeader()
    view.title = viewModel.titleForHeaderInSection(section)
    return view
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard let cell = tableView.cellForRow(at: indexPath) else {
      return
    }
    let success = viewModel.selectUserInterest(at: indexPath)
    cell.isSelectedStyle = success
  }
  
  override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    guard let cell = tableView.cellForRow(at: indexPath) else {
      return
    }
    let success = viewModel.deselectUserInterest(at: indexPath)
    cell.isSelectedStyle = !success
  }
  
  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 40
  }
  
  // MARK: Actions
  //==============
  @IBAction func actionBarButtonTapped() {
    let actionController = UIAlertController(
      title: String.localized.whatToDo, message: nil, preferredStyle: .actionSheet)
    let selectAllButton = UIAlertAction(
    title: String.localized.selectAll, style: .default) { _ in
      self.selectAllSubcategories()
    }
    let unselectAllButton = UIAlertAction(
    title: String.localized.unselectAll, style: .destructive) { _ in
      self.unselectAllSubcategories()
    }
    let saveButton = UIAlertAction(
    title: String.localized.save, style: .default) { _ in
      self.saveBarButtonTapped()
    }
    let cancelBarButton = UIAlertAction(
      title: String.localized.cancel, style: .cancel, handler: nil)
    actionController.addAction(selectAllButton)
    actionController.addAction(unselectAllButton)
    actionController.addAction(cancelBarButton)
    present(actionController, animated: true, completion: nil)
  }
  
  @IBAction func saveBarButtonTapped() {
    startLoader()
    viewModel.saveChanges { success -> Void in
      self.stopLoader()
      guard success else {
        return
      }
      self.refreshVisibleRows()
    }
  }
  
  // MARK: Helpers
  //==============
  func selectAllSubcategories() {
    viewModel.selectAllSubcategories()
    refreshVisibleRows()
  }
  
  func unselectAllSubcategories() {
    viewModel.deselectAllSubcategories()
    refreshVisibleRows()
  }
  
  func refreshVisibleRows() {
    guard let indexPathOfVisibleRows = tableView.indexPathsForVisibleRows else {
      return
    }
    tableView.reloadRows(at: indexPathOfVisibleRows, with: .none)
  }
}

// MARK: - UITableViewCell Extension
//=============================================================================
extension UITableViewCell {
  var isSelectedStyle: Bool {
    get {
      self.accessoryType == UITableViewCell.AccessoryType.checkmark
    }
    set {
      self.accessoryType = newValue ?
        UITableViewCell.AccessoryType.checkmark : UITableViewCell.AccessoryType.none
    }
  }
}

//
//  ViewController.swift
//  MobAdClient
//
//  Created by Marwan  on 7/25/19.
//  Copyright © 2019 Marwan Toutounji. All rights reserved.
//

import UIKit
import MobAdSDK
import UserNotifications

class MainViewController: UIViewController, MobAdStateDelegate {
    func onAdOpened(state adState: AdState) {
        print("Opened event")
    }
    
    func onAdClosed(state adState: AdState) {
        print("Closed event")

    }
    
    func onAdFailed(state adState: AdState) {
        print("Failed event")

    }
    
    func onMaxAdsOpenedReached(state adState: AdState) {
        print("Max Reached event")

    }
    
    func viewIsVisible() -> Bool {
        return MobAdSDK.shared.isViewVisible(self)
    }
    
  @IBOutlet weak var userInfoLabel: UILabel!
  @IBOutlet weak var settingsLabel: UILabel!
  @IBOutlet weak var showAdsLabel: UILabel!
  @IBOutlet weak var showAdsSwitch: UISwitch!
  @IBOutlet weak var numberOfAdsPerDayLabel: UILabel!
  @IBOutlet weak var numberOfAdsPerDayTextField: UITextField!
  @IBOutlet weak var interestsLabel: UILabel!
  @IBOutlet weak var interestsAdditionalInfoImageView: UIImageView!
  @IBOutlet weak var interestsAdditionalInfoImageViewHeight: NSLayoutConstraint!
  @IBOutlet weak var languagesLabel: UILabel!
  @IBOutlet weak var languagesAdditionalInfoImageView: UIImageView!
  @IBOutlet weak var languagesAdditionalInfoImageViewHeight: NSLayoutConstraint!
  @IBOutlet weak var carouselNotificationButton: UIButton!
  @IBOutlet weak var imageNotificationButton: UIButton!
  @IBOutlet weak var textNotificationButton: UIButton!
  @IBOutlet weak var videoNotificationButton: UIButton!
  
  let viewModel = MainViewModel()

  override func viewDidLoad() {
    super.viewDidLoad()
    configureViewController()
      MobAdSDK.shared.addListenerDelegate(self)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    displaySignInViewControllerIfNeeded()
    displayLocationAuthorizationRequestIfNeeded()
  }
    override func viewDidDisappear(_ animated: Bool) {
            MobAdSDK.shared.removeListenerDelegate(self)
        }

  
  // MARK: Actions
  //==============
  @IBAction func signOutButtonTapped() {
    signOut()
  }
  
  @IBAction func adServiceStatusSwitchTapped() {
    startLoader()
    viewModel.setAdServiceStatus(active: showAdsSwitch.isOn) { [weak self] success in
      guard let self = self else { return }
      self.stopLoader()
      self.showAdsSwitch.isOn = self.viewModel.adServiceActive
    }
  }
  
  @IBAction func interestsSectionTapped(_ gesture: UIGestureRecognizer) {
    animateSelected(gesture.view)
    show(InterestsTableViewController.instance, sender: self)
  }
  
  @IBAction func languagesSectionTapped(_ gesture: UIGestureRecognizer) {
    animateSelected(gesture.view)
    show(LanguagesViewController.instance, sender: self)
  }
  
  @IBAction func carouselNotificationButtonTapped() {
    viewModel.displayAdNotification(.carousel(nil))
  }
  
  @IBAction func imageNotificationButtonTapped() {
    viewModel.displayAdNotification(.image)
  }
  
  @IBAction func textNotificationButtonTapped() {
    viewModel.displayAdNotification(.text)
  }
  
  @IBAction func videoNotificationButtonTapped() {
    viewModel.displayAdNotification(.video)
  }
  
  @objc func didFinishEditingUserDailyCAP() {
    numberOfAdsPerDayTextField.resignFirstResponder()
    saveNumberOfAdsPerDay()
  }
  
  // MARK: Helpers
  //==============
  private func applyTheme() {
    settingsLabel.customizeAsPrimaryLabel()
    interestsLabel.customizeAsPrimaryLabel()
    languagesLabel.customizeAsPrimaryLabel()
    showAdsLabel.customizeAsDefaultLabel()
    numberOfAdsPerDayLabel.customizeAsDefaultLabel()
    carouselNotificationButton.customizeAsPrimaryButton()
    imageNotificationButton.customizeAsPrimaryButton()
    textNotificationButton.customizeAsPrimaryButton()
    videoNotificationButton.customizeAsPrimaryButton()
    
    userInfoLabel.textColor = Theme.highlightTextColor
    showAdsSwitch.onTintColor = Theme.highlightColor
    
    if #available(iOS 13.0, *) {
      let symbolConfig = UIImage.SymbolConfiguration(weight: .bold)
      let image = UIImage.init(systemName: "chevron.right", withConfiguration: symbolConfig)
      interestsAdditionalInfoImageView.image = image
      interestsAdditionalInfoImageViewHeight.constant = 30
      interestsAdditionalInfoImageView.layoutIfNeeded()
      languagesAdditionalInfoImageView.image = image
      languagesAdditionalInfoImageViewHeight.constant = 30
      languagesAdditionalInfoImageView.layoutIfNeeded()
    } else {
      let image = UIImage(named: "chevron.right")
      interestsAdditionalInfoImageView.image = image
      interestsAdditionalInfoImageViewHeight.constant = 20
      interestsAdditionalInfoImageView.layoutIfNeeded()
      languagesAdditionalInfoImageView.image = image
      languagesAdditionalInfoImageViewHeight.constant = 20
      languagesAdditionalInfoImageView.layoutIfNeeded()
    }
  }
  
  private func configureViewController() {
    configureNumberOfAdsDailyTextField()
    refreshAdServiceStatusSwitch()
    refreshUserInfoLabel()
    refreshNumberOfAdsPerDayTextField()
    applyTheme()
  }
  
  private func configureNumberOfAdsDailyTextField() {
    let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 45) )
    toolbar.barStyle = UIBarStyle.default
    toolbar.items = [
      UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
      UIBarButtonItem(title: String.localized.done, style: .done, target: self, action: #selector(didFinishEditingUserDailyCAP))
    ]
    numberOfAdsPerDayTextField.inputAccessoryView = toolbar
  }
  
  private func refreshAdServiceStatusSwitch() {
    showAdsSwitch.isOn = viewModel.adServiceActive
  }
  
  private func refreshUserInfoLabel() {
    userInfoLabel.text = viewModel.userInfoLabelContent
  }
  
  private func refreshNumberOfAdsPerDayTextField() {
    numberOfAdsPerDayTextField.text = "\(viewModel.maximumAdsPerDay)"
  }
  
  private func displayLocationAuthorizationRequestIfNeeded() {
    guard viewModel.shouldDisplayLocationPermission else {
      return
    }
    MobAdSDK.shared.requestAlwaysAuthorizationForLocationMonitoring()
    MobAdSDK.shared.activate(for: [.call, .locationChange])
  }
  
  private func displaySignInViewControllerIfNeeded() {
    if !viewModel.isSignedIn {
      displaySignInViewController()
    }
  }
  
  private func animateSelected(_ view: UIView?) {
    view?.backgroundColor = UIColor.lightGray
    UIView.animate(withDuration: 0.4) {
      view?.backgroundColor = UIColor.clear
    }
  }
  
  func displaySignInViewController() {
    guard let viewController = storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as? SignInViewController else {
      return
    }
    viewController.delegate = self
    present(viewController, animated: true, completion: nil)
  }
  
  private func saveNumberOfAdsPerDay() {
    guard let stringValue = numberOfAdsPerDayTextField.text,
      let intValue = Int(stringValue) else {
      return
    }
    startLoader()
    viewModel.setMaximumAdsPerDay(cap: intValue) { success, maxAdsPerDay in
      if let maxAdsPerDay = maxAdsPerDay {
        self.numberOfAdsPerDayTextField.text = "\(maxAdsPerDay)"
      }
      self.stopLoader()
    }
  }
  
  private func signOut() {
    viewModel.signOutUser()
    displaySignInViewControllerIfNeeded()
    refreshUserInfoLabel()
  }
}

// MARK: - Sign In Delegate
extension MainViewController: SignInViewControllerDelegate {
  func signInDidFinish(success: Bool) {
    refreshUserInfoLabel()
    if success {
      dismiss(animated: true, completion: nil)
    }
  }
}

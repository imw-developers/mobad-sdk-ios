//
//  MainViewModel.swift
//  MobAdClient
//
//  Created by Marwan  on 12/13/19.
//  Copyright © 2019 Marwan Toutounji. All rights reserved.
//

import Foundation
import MobAdSDK

class MainViewModel {
  var isSignedIn: Bool {
    MobAdSDK.shared.isSignedIn
  }
  
  var adServiceActive: Bool {
    return MobAdSDK.shared.adServiceActive
  }
  
  private var user: User? {
    return MobAdSDK.shared.user
  }
  
  var userInfoLabelContent: String {
    if isSignedIn {
      let email = user?.email ?? String.localized.guest.uppercased()
      return String.localized.signedInUserMessage(user: email)
    } else {
      return String.localized.pleaseSignInGuest
    }
  }
  
  func signOutUser() {
    MobAdSDK.shared.signOutUser()
  }
  
  func displayAdNotification(_ mediaType: MediaType) {
    MobAdSDK.shared.scheduleAdNotification(after: 3, mediaType: mediaType)
  }
  
  var maximumAdsPerDay: Int {
    return MobAdSDK.shared.userMaximumAdsPerDay
  }
  
  func setMaximumAdsPerDay(cap: Int, completion: @escaping (_ success: Bool, _ maxAdsPerDay: Int?) -> Void) {
    MobAdSDK.shared.updateUserProfile(countryCode: nil, languageCode: nil, maxAdsPerDay: cap, preferredAdLanguagesCodes: nil) { (success, maxAdsPerDay, error) in
      completion(success, maxAdsPerDay)
    }
  }
  
  func setAdServiceStatus(active: Bool, completion: @escaping (_ success: Bool) -> Void) {
    MobAdSDK.shared.adService(activate: active) { (success, error) in
      completion(success)
    }
  }
  
  var shouldDisplayLocationPermission: Bool {
    return MobAdSDK.shared.canAskPermissionForAlwaysMonitoringLocation()
  }
}

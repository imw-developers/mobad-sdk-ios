//
//  UIViewController+Extension.swift
//  MobAdClient
//
//  Created by Marwan  on 1/15/20.
//  Copyright © 2020 Marwan Toutounji. All rights reserved.
//

import UIKit

extension UIViewController: NVActivityIndicatorViewable {
  func startLoader() {
    startAnimating(type: NVActivityIndicatorType.pacman, color: UIColor.white)
  }
  
  func stopLoader() {
    stopAnimating()
  }
}

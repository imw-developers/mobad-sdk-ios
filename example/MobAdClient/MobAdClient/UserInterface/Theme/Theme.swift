//
//  Theme.swift
//  MobAdClient
//
//  Created by Marwan  on 12/18/19.
//  Copyright © 2019 Marwan Toutounji. All rights reserved.
//

import Foundation
import UIKit

class Theme {
  static func apply() {
    let barButtonAppearance = UIBarButtonItem.appearance()
    barButtonAppearance.tintColor = UIColor.orange
    
    let navigationBarAppearance = UINavigationBar.appearance()
    navigationBarAppearance.tintColor = UIColor.orange
  }
  
  static var highlightColor: UIColor? {
    return UIColor.orange
  }
  
  static var highlightTextColor: UIColor? {
    return highlightColor
  }
}

extension UIButton {
  func customizeAsPrimaryButton() {
    self.backgroundColor = UIColor.orange
    self.tintColor = UIColor.white
    self.layer.cornerRadius = 5.0
  }
  
  func customizeAsSecondaryButton() {
    self.backgroundColor = UIColor.white
    self.tintColor = UIColor.gray
  }
}

extension UILabel {
  func customizeAsPrimaryLabel() {
    self.font = UIFont.boldSystemFont(ofSize: 30)
  }
  
  func customizeAsDefaultLabel() {
    self.font = UIFont.systemFont(ofSize: 20)
  }
}

extension UIColor {
  static var orange: UIColor? {
    if #available(iOS 11.0, *) {
      return UIColor(named: "Orange")
    } else {
      return UIColor(displayP3Red: 0.89, green: 0.576, blue: 0.294, alpha: 1)
    }
  }
  static var lightGray: UIColor? {
    if #available(iOS 11.0, *) {
      return UIColor(named: "LightGray")
    } else {
      return UIColor(displayP3Red: 0.799, green: 0.799, blue: 0.799, alpha: 1)
    }
  }
}

//
//  SpacerView.swift
//  MobAdClient
//
//  Created by Marwan Toutounji on 4/2/20.
//  Copyright © 2020 Marwan Toutounji. All rights reserved.
//

import UIKit

@IBDesignable
class SpacerView : UIView {
  
  @IBInspectable
  var verticle: CGFloat = 10 {
    didSet {
      invalidateIntrinsicContentSize()
    }
  }
  
  @IBInspectable
  var horizontal: CGFloat = 10 {
    didSet {
      invalidateIntrinsicContentSize()
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
//    commonSetup()
  }
  
  override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
//    commonSetup()
  }
  
  override var intrinsicContentSize: CGSize {
    return CGSize(width: horizontal, height: verticle)
  }
}

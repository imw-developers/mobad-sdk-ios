//
//  NotificationViewController.swift
//  ClientImageNotificationExtension
//
//  Created by Marwan  on 8/15/19.
//  Copyright © 2019 Marwan Toutounji. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: UIViewController, UNNotificationContentExtension {
  @IBOutlet var imageView : UIImageView?
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  func didReceive(_ notification: UNNotification) {
    guard let imageURLString = notification.request.content.userInfo["image"] as? String,
      let url = URL(string: imageURLString) else {
        return
    }
    
    guard let data = try? Data(contentsOf: url) else {
      return
    }
    
    let image = UIImage(data: data)
    self.imageView?.image = image
  }
}
